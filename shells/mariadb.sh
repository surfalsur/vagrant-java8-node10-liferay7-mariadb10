#!/bin/bash

apt update -y
sudo apt install mariadb-server mariadb-client -y

sudo mysql -e "CREATE DATABASE cemexevo DEFAULT CHARACTER SET utf8;"
sudo mysql -e "CREATE USER 'cemexevo'@'localhost' IDENTIFIED BY 'cemexevo';"
sudo mysql -e "GRANT ALL ON cemexevo.* TO 'cemexevo'@'%';"

##permission to all ips
echo "bind-address=0.0.0.0" | sudo tee -a /etc/mysql/mariadb.cnf

sudo service mysql restart;




