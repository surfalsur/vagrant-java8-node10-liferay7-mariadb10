#!/usr/bin/env bash

## LIFERAY 7 PROTAL

export LIFERAY_HOME=/home/vagrant/liferay
export CATALINA_HOME=$LIFERAY_HOME/tomcat-8.0.32
export PATH=$CATALINA_HOME/bin:$PATH

curl -O -k -L https://sourceforge.net/projects/lportal/files/Liferay%20Portal/7.0.3%20GA4/liferay-ce-portal-tomcat-7.0-ga4-20170613175008905.zip && \
unzip /home/vagrant/liferay-ce-portal-tomcat-7.0-ga4-20170613175008905.zip -d /home/vagrant && mv /home/vagrant/liferay-ce-portal-7.0-ga4 /home/vagrant/liferay \
 && chown -R vagrant:vagrant $LIFERAY_HOME

touch $LIFERAY_HOME/portal-ext.properties

echo "admin.email.from.address=joaquincabal@gmail.com
nadmin.email.from.name=Joaquin Cabal
nusers.reminder.queries.enabled=false
terms.of.use.required=false
liferay.home=$LIFERAY_HOME
setup.wizard.enabled=false
jdbc.default.driverClassName=org.mariadb.jdbc.Driver
jdbc.default.url=jdbc:mariadb://localhost/cemexevo?useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false
jdbc.default.username=cemexevo
jdbc.default.password=cemexevo" >> $LIFERAY_HOME/portal-ext.properties  


#blade cli
curl -O -k -L https://raw.githubusercontent.com/jpm4j/jpm4j.installers/master/dist/biz.aQute.jpm.run.jar
sudo mv biz.aQute.jpm.run.jar /home/vagrant/biz.aQute.jpm.run.jar
sudo chmod vagrant:vagrant biz.aQute.jpm.run.jar
chmod +x /home/vagrant/biz.aQute.jpm.run.jar
sudo java -jar /home/vagrant/biz.aQute.jpm.run.jar init
sudo jpm install -f https://releases.liferay.com/tools/blade-cli/latest/blade.jar
sudo blade update

