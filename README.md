Install vagrant VM with the following

Java 8 - Openjdk
Liferay 7.0
Node 10.x
Git - Git gui
Mariadb10


For start vm, place in Vagrantfile folder location and run "vagrant up"
For enter ssh vm, place in Vagrantfile folder and run "vagrant ssh". 
Once in vm you can go to shared folder /vagrant. There you can see all folders placed in Vagrantfile folder.
There you can place your projects. So you can edit your project in your host machine.

[Windows] To fully run "mvn clean install" without node_modules symlink errors,
you have to move your git repository into the VM's filesystem.
Recomendation: git clone inside "/home/vagrant"

You can exit vm with "exit"
You can stop vm with "vagrant halt".
